package org.medtech.restservicestarter.services;

import jakarta.transaction.Transactional;
import org.medtech.restservicestarter.converter.GenericDualConverter;
import org.medtech.restservicestarter.converter.GenericSimpleConverter;
import org.springframework.data.jpa.repository.JpaRepository;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;


public abstract class AbstractGenericService<T,DTO ,ID extends Serializable, R extends JpaRepository<T, ID>>
        implements CrudService<T, ID> {
    protected final R repository;

    protected AbstractGenericService(R repository) {
        this.repository = repository;
    }
    @Override
    @Transactional
    public <S extends T> S save(S entity) {return repository.save(entity);}

    @Transactional
    public <S extends T> DTO saveWithConversion(DTO dto, GenericDualConverter<S, DTO> converter ) {
        return converter.convert( repository.save( converter.reverse(dto)) );
    }

    public Optional<T> findById(ID id) {
        return repository.findById(id);
    }
    @Override
    @Transactional
    public void delete(T entity) {
        repository.delete(entity);
    }
    @Override
    public List<T> findAll() {
        return repository.findAll();
    }
    @Override
    public <S extends T> List<S> saveAll(Iterable<S> entities) {return repository.saveAll(entities);}
}
