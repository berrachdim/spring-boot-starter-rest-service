package org.medtech.restservicestarter.services.action;

@FunctionalInterface
public interface Action<T> {
    void execute(T entity);
}
