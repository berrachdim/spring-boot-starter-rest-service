package org.medtech.restservicestarter.services;

import org.medtech.restservicestarter.services.action.Action;
import org.springframework.data.repository.CrudRepository;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

public interface CrudService <T, ID extends Serializable> {
    <S extends T> S save(S entity);
    <S extends T> List<S> saveAll(Iterable<S> entities);
    Optional<T> findById(ID id);
    List<T> findAll();
    void delete(T entity);

}
