package org.medtech.restservicestarter.converter;

import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

public abstract class GenericDualConverter<S,DTO> extends GenericSimpleConverter<S,DTO> {
    protected abstract Function<DTO,S> fromTarget();
    public S reverse(DTO dto){
        return Objects.isNull(dto) ? null : fromTarget().apply(dto);
    }
    public List<S> reversAll(List<DTO> targets){
        if(CollectionUtils.isEmpty(targets)) return Collections.emptyList();
        return targets.stream()
                .map(t->fromTarget().apply(t))
                .collect(Collectors.toList());
    }

}
