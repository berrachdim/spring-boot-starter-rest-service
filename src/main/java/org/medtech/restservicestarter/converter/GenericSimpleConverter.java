package org.medtech.restservicestarter.converter;

import org.springframework.util.CollectionUtils;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

public abstract class GenericSimpleConverter<S,DTO> {
    protected abstract Function<S,DTO> fromSource();
    public DTO convert(S source){
        return Objects.isNull(source) ? null : fromSource().apply(source);
    }
    public List<DTO> convertAll(List<S> sources){
        if(CollectionUtils.isEmpty(sources)) return Collections.emptyList();
        return sources.stream()
                .map(s->fromSource().apply(s))
                .collect(Collectors.toList());
    }
}
