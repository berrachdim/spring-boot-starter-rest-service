package org.medtech.restservicestarter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestServiceStarterApplication {

    public static void main(String[] args) {
        SpringApplication.run(RestServiceStarterApplication.class, args);
    }

}
